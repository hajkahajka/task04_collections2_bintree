public class Node<K extends Comparable,V> implements Comparable<Node<K,V>>{

    private K key;
    private V value;
    private Node leftN;
    private Node rightN;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", value=" + value +
                ", leftN=" + leftN +
                ", rightN=" + rightN +
                '}';
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public Node getLeftN() {
        return leftN;
    }

    public void setLeftN(Node leftN) {
        this.leftN = leftN;
    }

    public Node getRightN() {
        return rightN;
    }

    public void setRightN(Node rightN) {
        this.rightN = rightN;
    }

    @Override
    public int compareTo(Node<K, V> o) {
        return 0;
    }
}
