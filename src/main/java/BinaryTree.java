import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BinaryTree<K extends Comparable, V> implements Map {

    private int size;
    private Node<K,V> node;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Node<K, V> getNode() {
        return node;
    }

    public void setNode(Node<K, V> node) {
        this.node = node;
    }

    public BinaryTree() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (node == null);
    }

    @Override
    public boolean containsKey(Object key) {

        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public Object get(Object key) {
        return node;
    }

    @Override
    public Object put(Object key, Object value) {

        return null;
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set keySet() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }

    @Override
    public Set<Entry> entrySet() {
        return null;
    }
}
