public class Test {
    public static void main(String[] args){
        BinaryTree<Integer,String> binaryTree = new BinaryTree<>();

        binaryTree.put(1,"Wolf");
        binaryTree.put(2,"Squirrel");
        binaryTree.put(3,"Dog");
        binaryTree.put(1,"Cat");

        Node<?,?> node = binaryTree.getNode();

//    System.out.println(node.getKey());
//    System.out.println(node.getValue());

        System.out.println(node.toString());

//    System.out.println("Left node key - "+ node.getLeftNode().getKey());
//    System.out.println("Left node value - "+node.getLeftNode().getValue().toString());
//
    System.out.println("Right node:/n "+ node.getRightN().getKey()+ node.getRightN().getValue().toString());

    }

}
